/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


let PLAYER_FORCE = {Right:12,Left:-12};

const STATE = {MainMenu:0,GamePlaying:1,GameOver:2,AskIfReplay:3,Score:4,HowToPlay:5};

const ITEM_TYPE = {PlayerSpeedUp:0,SlowTime:1,BonusLiquors:2};

const LIQUOR_TYPE = {Soju:0,Maekju:1,Sake:2,Wine:3,SogokJu:4,Whiskey:5,Bonus:6};

const Emotion = {Normal:0,Happy:1,Mad:2};

const CATEGORY_PLAYER = 0x0002;

const CATEGORY_SWORD = 0x0004;

const CATEGORY_ITEM = 0x0008;

const CATEGORY_LIQUORS = 0x0020;

const CATEGORY_GROUND = 0x0040;

const CATEGORY_PARTICLES = 0x0080;

const CATEGORY_BASKET = 0x0200;
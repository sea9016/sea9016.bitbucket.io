/*

Doyoon Kim (김도윤)
 
assignment : Prototype

C//S099

Spring 2019 

*/
//

class Particle
{
  constructor(x,y,w,h,mass,img)
  {
     this.LifeSpan = 400;
     this.W = w;
     this.H = h;
     this.Img = img;
     this.Body = Bodies.rectangle(x,y,w,h,{collisionFilter:{category:CATEGORY_PARTICLES,mask:CATEGORY_PARTICLES|CATEGORY_SWORD|CATEGORY_GROUND|CATEGORY_LIQUORS|CATEGORY_BASKET},mass:mass});
     Matter.Body.set(this.Body,{angularVelocity:random(-1,1),velocity:{x:random(-2,2),y:random(-2,-3)}});
  }

  Update()
  {
  	if(this.LifeSpan>0)
  	{
  	  this.LifeSpan--;
      if(this.Body.position.x>width&&this.Body.velocity.x>0||this.Body.position.x<0&&this.Body.velocity.x<0)
      {
        Matter.Body.set(this.Body,{velocity:{x:this.Body.velocity.x*-1,y:this.Body.velocity.y}});
      }
  	}
  	else
  	{
  	  this.LifeSpan = 0;
  	}
  }

  Draw()
  {
  	push();
  	translate(this.Body.position.x,this.Body.position.y);
  	rotate(this.Body.angle);
    fill(0,150);
  	// rect(0,0,this.W,this.H);// will be replaced by image();
    image(this.Img,0,0,this.W,this.H);
  	pop();
  }
}
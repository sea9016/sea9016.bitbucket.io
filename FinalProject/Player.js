/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/



class Player
{
  constructor(x,y,w,h)
  { 
    this.W = w;
    this.H = h;
    this.NormalImg = loadImage('Images/Character.png');
    this.MadImg = loadImage('Images/Character_Mad.png');
    this.HappyImg = loadImage('Images/Character_Happy.png');
    this.Body = Bodies.rectangle(x,y,w,h,{collisionFilter:{category:CATEGORY_PLAYER,mask :CATEGORY_ITEM | CATEGORY_LIQUORS | CATEGORY_GROUND},friction:0.8,inertia:Infinity,inverseInertia:1/Infinity,mass:200,owner:this});
    this.IsHoldingBasket = false;
    this.Emotion = Emotion.Normal;
    this.InitEmotionDuration = 100;
    this.EmotionDuration = this.InitEmotionDuration; 

    this.Sword = new Sword(this.Body.position.x,this.Body.position.y-this.H/2-this.H-20,this.W/3,this.H*1.5);
    this.Basket = new Basket(this.Body.position.x,height-this.H-this.H/8,this.W*1.2,this.H/6);
  }
  
  Draw()
  {
    switch(this.Emotion)
    {
      case Emotion.Normal:
      image(this.NormalImg,this.Body.position.x,this.Body.position.y,this.W,this.H);
      break;
      case Emotion.Happy:
      image(this.HappyImg,this.Body.position.x,this.Body.position.y,this.W,this.H);
      break;
      case Emotion.Mad:
      image(this.MadImg,this.Body.position.x,this.Body.position.y,this.W,this.H);
      break;
      default:
      break;
    }

    if(this.IsHoldingBasket)
    {
      this.Basket.Draw();
    }

    this.Sword.Draw();
  }      

  GetInput()
  { 
    if(keyIsDown(65))//a
    {
      Matter.Body.applyForce(this.Body,this.Body.position,{x:PLAYER_FORCE.Left,y:0});
    }
    else if(keyIsDown(68))//d
    {

      Matter.Body.applyForce(this.Body,this.Body.position,{x:PLAYER_FORCE.Right,y:0});
    }

    if(keyIsDown(LEFT_ARROW))
    {
      Matter.Body.set(this.Sword.Body,{angularVelocity:-0.25});
    }
    else if(keyIsDown(RIGHT_ARROW))
    {
      Matter.Body.set(this.Sword.Body,{angularVelocity:0.25});
    }

    if(keyIsDown(32))//spacebar
    {
      this.IsHoldingBasket = true;
    }
    else
    {
      this.IsHoldingBasket = false;
    }
  }

  Scream()
  {
    this.Emotion = Emotion.Mad;
    this.EmotionDuration = this.InitEmotionDuration;
  }

  HappyReaction()
  {
    this.Emotion = Emotion.Happy;
    this.EmotionDuration = this.InitEmotionDuration;
    HappyReactionSound.play();
  }

  Update()
  {
    Matter.Body.set(this.Body,{position:{x:this.Body.position.x,y:height-200-this.H/2}});
    if(this.Body.position.x-this.W/2<0)
    {
      Matter.Body.set(this.Body,{position:{x:this.W/2,y:this.Body.position.y}});
    }
    else if(this.Body.position.x+this.W/2>width)
    {
      Matter.Body.set(this.Body,{position:{x:width-this.W/2,y:this.Body.position.y}});
    }
    Matter.Body.set(this.Body,{velocity:{x:this.Body.velocity.x*0.5,y:0}});
    this.Sword.Update();

    this.EmotionDuration--;
    if(this.EmotionDuration <= 0)
    {
      this.Emotion = Emotion.Normal;
    }
  }
}

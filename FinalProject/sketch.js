/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


//module allias
let Engine = Matter.Engine; 
let Bodies = Matter.Bodies;
let World = Matter.World;
let Constraint = Matter.Constraint;
let Events = Matter.Events;
// let Composite = Matter.Composite;

//create a engine
let engine;
  //create two boxes and a ground
let boxA;
let boxB;
let ground;
let constraint_P_S;
let constraint_P_B;
let compositePlayer;


let _Player;
let _Sword;
let _Basket;
let _ObjectManager;
let _ParticleManager;
let _Game;
let _UI;
let PrevTime = 0

let bg;
let HowToPlayImg;

let bgm;
// let ScreamSound;
let HappyReactionSound;
let GlassBreakSound;

function preload()
{
  GlassBreakSound = loadSound('Sounds/GlassBreak.mp3');
  bgm = loadSound('Sounds/Bgm.wav');
  HappyReactionSound = loadSound('Sounds/Yay.mp3');
  // ScreamSound = loadSound('Sounds/Scream.m4a');
}

function setup() {
  createCanvas(800, 1000);
  rectMode(CENTER);
  imageMode(CENTER);
  textSize(20);
  frameRate(60);
  bgm.loop();
  
  engine = Engine.create();
  ground = Bodies.rectangle(width/2,height-100,800,200,{
    collisionFilter:{category:CATEGORY_GROUND,mask:CATEGORY_PLAYER|CATEGORY_LIQUORS|CATEGORY_ITEM|CATEGORY_PARTICLES}
    ,friction:0.9
    ,isStatic:true
  });
 
  _Player = new Player(width/2,height-200-height/16,width/10,height/8);

  _ObjectManager = new ObjectManager();
  _ParticleManager = new ParticleManager();
  _Game = new Game();
  _Game.State = STATE.MainMenu;
  _UI = new UI();
  
   let option_P_S = {
   bodyA : _Player.Body,
   bodyB : _Player.Sword.Body,
   pointA:{x:0,y:0},
   pointB:{x:0,y:_Player.Sword.H/2},
   length: 1,
   angularStiffness:1,
   stiffness:1
  }

  constraint_P_S = Constraint.create(option_P_S);

   let option_P_B = {
   bodyA:_Player.Body,
   bodyB:_Player.Basket.Body,
   pointA:{x:0,y:-_Player.H/2},
   pointB:{x:0,y:_Player.Basket.H/2},
   length: 1,
   angularStiffness : 1,
   stiffness : 1
  }
  
  constraint_P_B = Constraint.create(option_P_B);

  World.add(engine.world,[ground,_Player.Body,_Player.Sword.Body,_Player.Basket.Body,constraint_P_S,constraint_P_B]);
  Engine.run(engine);
  CollisionEventUpdate();

  bg = loadImage('Images/Background.jpg');
  HowToPlayImg = loadImage('Images/How to play.png');
}

function draw() {
  frameRate(200);
  image(bg,width/2,height/2,1000,1000);
  switch(_Game.State)
  {
    case STATE.MainMenu:
    break;

    case STATE.GamePlaying:
    GameLoop();
    break;

    case STATE.GameOver:
    push();
    textAlign(CENTER);
    textSize(30);
    text('GameOver!',width/2,height/2);
    pop();
    setTimeout(function(){
      if(_Game.State == STATE.GameOver)
      {
        _Game.State = STATE.AskIfReplay;
        _UI.ReplayButton.show();
        _UI.BackToMenuButton.show();
      }
    },2000);
    break;

    case STATE.AskIfReplay:
    push();
    textAlign(CENTER);
    textSize(20);
    text('Replay?',width/2,height/3);
    pop();
    break;

    case STATE.Score:
    break;

    default:
    break;
  }

    _UI.Draw();
}

function GameLoop()
{
  _Player.GetInput();

  _ObjectManager.Update();
  _ParticleManager.Update();
  _Player.Update();

  _ObjectManager.DrawObjects();
  _ParticleManager.DrawParticles();
  _Player.Draw();
  
  _Game.CheckState();
}

function CollisionEventUpdate()
{
  Events.on(engine,'collisionStart',function(event){
    let CollisionPairs = event.pairs;
    for(let i = 0; i<CollisionPairs.length;++i)
    {
      let pair = CollisionPairs[i];

      if(pair.bodyA.collisionFilter.category == CATEGORY_LIQUORS)
      {
        if(pair.bodyB.collisionFilter.category == CATEGORY_SWORD)
        {
           pair.bodyA.owner.DecreaseLife();
        }
        else if(pair.bodyB.collisionFilter.category == CATEGORY_BASKET)
        {
          if(_Player.IsHoldingBasket)
          {
            pair.bodyA.owner.IsCollected = true;
          }
        }
      }
      else if(pair.bodyB.collisionFilter.category == CATEGORY_LIQUORS)
      {
        if(pair.bodyA.collisionFilter.category == CATEGORY_SWORD)
        {
           pair.bodyB.owner.DecreaseLife();
        }
        else if(pair.bodyA.collisionFilter.category == CATEGORY_BASKET)
        {
          if(_Player.IsHoldingBasket)
          {
            pair.bodyB.owner.IsCollected = true;
          }
        }
      }
      else if(pair.bodyA.collisionFilter.category == CATEGORY_ITEM)
      {
        if(pair.bodyB.collisionFilter.category == CATEGORY_PLAYER)
        {
           pair.bodyA.owner.Consume();  
        }
      }
      else if(pair.bodyB.collisionFilter.category == CATEGORY_ITEM)
      {
        if(pair.bodyA.collisionFilter.category == CATEGORY_PLAYER)
        {
           pair.bodyB.owner.Consume();
        }
      }
    }
  });
}
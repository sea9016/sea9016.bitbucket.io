/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class ObjectManager
{
  constructor()
  {
    this.LiquorList = [];
    this.ItemList = [];
    this.InitSpawnDelay = 150;
    this.SpawnDelay = this.InitSpawnDelay;
  }
  
  SpawnLiquor()
  {
    if(_Game.State == STATE.GamePlaying&&this.SpawnDelay == 0)
    {
      let LiquorType = floor(random(0,6));
      let NewLiquor = new Liquor(random(0,width*4/5),0,width/15,height/10,3,LiquorType);
      NewLiquor.Body.owner = NewLiquor;
      World.add(engine.world,NewLiquor.Body);
      this.LiquorList.push(NewLiquor);
    }
  }

  SpawnBonusLiquor()
  {
    for(let i = 0; i<10; i++)
    {
      let NewLiquor = new Liquor(random(width/3,width*2/3),0,width/15,height/10,1,LIQUOR_TYPE.Bonus);
      NewLiquor.Body.owner = NewLiquor;
      World.add(engine.world,NewLiquor.Body);
      this.LiquorList.push(NewLiquor);
    }
  }

  SpawnItem()
  {
    let ItemType = floor(random(0,3));
    let NewItem = new Item(random(0,width),0,ItemType);
    NewItem.Body.owner = NewItem;
    World.add(engine.world,NewItem.Body);
    this.ItemList.push(NewItem);
  }

  UpdateItems()
  {
    for(let i in this.ItemList)
    {
      let Item = this.ItemList[i];
      if(Item.Body.position.y+height/30>height-200)
      {
        this.ItemList.splice(i,1);
      }
      if(Item.IsUsed)
      {
        this.ItemList.splice(i,1);
      }
    }
  }
  
  UpdateLiquors()
  {
    for(let i in this.LiquorList)
    {
      let Liquor = this.LiquorList[i];
      Liquor.Update();
      if(Liquor.Body.position.y+Liquor.H*0.6>height-200)//땅에 떨어지면
      {
       //파티클 매니저에다가 파티클 만들라고 시킴.
        // console.log('파티클 스폰!');
        if(Liquor.Type!=LIQUOR_TYPE.Bonus)
        {
          _Game.Life-=200;
          _Player.Scream();
        }
        _ParticleManager.SpawnParticle(Liquor.Body.position.x,Liquor.Body.position.y,Liquor.W/3,Liquor.H/3,Liquor.Body.mass/6,Liquor.Type);
        // GlassBreakSound.stop();
        GlassBreakSound.play();
        World.remove(engine.world,Liquor.Body);
        this.LiquorList.splice(i,1);
      }
      else if(Liquor.Life == 0)//liquor 라이프 0이면
      {  
        if(Liquor.Type == LIQUOR_TYPE.Soju||Liquor.Type == LIQUOR_TYPE.Maekju||Liquor.Type == LIQUOR_TYPE.Bonus)
        {
          _Game.Life+=100;
          _Player.HappyReaction();
          if(Liquor.IsHoldingItem&&Liquor.Type != LIQUOR_TYPE.Bonus)
          {
            this.SpawnItem();
          }
        }
        else 
        {
          _Game.Life-=200;
          _Player.Scream();
        }
        _ParticleManager.SpawnParticle(Liquor.Body.position.x,Liquor.Body.position.y,Liquor.W/3,Liquor.H/3,Liquor.Body.mass/6,Liquor.Type);
        // GlassBreakSound.stop();
        GlassBreakSound.play();
        World.remove(engine.world,Liquor.Body);
        this.LiquorList.splice(i,1);
      }
      else if(Liquor.IsCollected)// 바구니로 받아 먹으면 
      {
        if(Liquor.Type == LIQUOR_TYPE.Soju||Liquor.Type == LIQUOR_TYPE.Maekju)
        {
          _Game.Life-=200;
          _Player.Scream();
        }
        else if(Liquor.Type == LIQUOR_TYPE.Bonus)
        {
          //nothing happens.
        }
        else
        {
          _Game.Life+=100;
          _Player.HappyReaction();
          if(Liquor.IsHoldingItem)
          {
            this.SpawnItem();
          }
        }
        World.remove(engine.world,Liquor.Body);
        this.LiquorList.splice(i,1);
      }
    }
  }
  
  UpdateSpawnDelay()
  {
    if(this.SpawnDelay>0)
    {
      this.SpawnDelay--;
    }
    else
    {
      this.SpawnDelay = this.InitSpawnDelay;
    }
  }


  Update()
  {
    this.UpdateSpawnDelay();
    this.SpawnLiquor();
    this.UpdateLiquors();
    this.UpdateItems();
  }
  
  DrawObjects()
  {
    for(let Liquor of this.LiquorList)
    {
      Liquor.Draw();
    }

    for(let Item of this.ItemList)
    {
      Item.Draw();
    }
  }
  
  Reset()
  {
    this.LiquorList = [];
    this.ItemList = [];
  }
}
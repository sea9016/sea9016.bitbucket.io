/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class ParticleManager
{
  constructor()
  {
    this.ParticleArr = [];
  }
  
  SpawnParticle(x,y,w,h,mass,type)
  {
    let NewParticle1
    let NewParticle2
    let NewParticle3
    let NewParticle4
    let NewParticle5
    let NewParticle6
    switch(type)
    {
      case LIQUOR_TYPE.Soju:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/SojuParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.Maekju:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/MaekjuParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.Sake:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/SakeParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.Wine:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/WineParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.SogokJu:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/SogokjuParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.Whiskey:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyuParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyuParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/WhiskeyParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      case LIQUOR_TYPE.Bonus:
      NewParticle1 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle1.png'));
      NewParticle2 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle2.png'));
      NewParticle3 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle3.png'));
      NewParticle4 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle4.png'));
      NewParticle5 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle5.png'));
      NewParticle6 = new Particle(x,y,w,h,mass,loadImage('Images/BonusLiquorParticle6.png'));
      this.ParticleArr.push(NewParticle1);
      this.ParticleArr.push(NewParticle2);
      this.ParticleArr.push(NewParticle3);
      this.ParticleArr.push(NewParticle4);
      this.ParticleArr.push(NewParticle5);
      this.ParticleArr.push(NewParticle6);
      World.add(engine.world,[NewParticle1.Body,NewParticle2.Body,NewParticle3.Body,NewParticle4.Body,NewParticle5.Body,NewParticle6.Body]);
      break;
      default:
      break;
    }
  }
  
  Update()
  {
    for(let i in this.ParticleArr)
    {
      this.ParticleArr[i].Update();
      if(this.ParticleArr[i].LifeSpan == 0)
      {
        World.remove(engine.world,this.ParticleArr[i].Body);
        this.ParticleArr.splice(i,1);
      }
    }
  }
  
  DrawParticles()
  {
    for(let Particle of this.ParticleArr)
    {
      Particle.Draw();
    }
  }

  Reset()
  {
    this.ParticleArr = [];
  }
}
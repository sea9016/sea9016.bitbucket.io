/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class Item
{
  constructor(x,y,type)
  {
  	this.Type = type;//will be fixed.
  	this.Image = null;
    this.IsUsed = false;
  	switch(type)
  	{
  	  case ITEM_TYPE.PlayerSpeedUp:
      this.Image = loadImage('Images/Speedup Item.png');
  	  break;
  	  case ITEM_TYPE.SlowTime:
      this.Image = loadImage('Images/Slowtime item.png');
  	  break;
  	  case ITEM_TYPE.BonusLiquors:
      this.Image = loadImage('Images/Bonusliquors Item.png');
  	  break;
  	  default:
  	  break;
  	}
    this.X = x;
    this.Y = y;
    this.W = width/10;
    this.H = width/10;
    this.Body = Bodies.rectangle(x,y,this.W,this.H,{collisionFilter:{category:CATEGORY_ITEM,mask:CATEGORY_PLAYER},isSensor:true,owner:this});
    Matter.Body.set(this.Body,{velocity:{x:0,y:random(-6,-4)}});

  }
  
  Consume()
  {
  	switch(this.Type)
  	{
      case ITEM_TYPE.PlayerSpeedUp:
      PLAYER_FORCE.Right*=1.4;
      PLAYER_FORCE.Left*=1.4;
      setTimeout(function(){
        PLAYER_FORCE.Right/=1.4;
        PLAYER_FORCE.Left/=1.4;
      },15000);
      break;
      case ITEM_TYPE.SlowTime:
      engine.world.gravity.scale/=5;
      setTimeout(function(){
        engine.world.gravity.scale*=5;
      },10000);
      break;
      case ITEM_TYPE.BonusLiquors:
      _ObjectManager.SpawnBonusLiquor();
      break;
      default:
      break;
  	}
  	this.IsUsed = true;
  }

  Draw()
  {
    image(this.Image,this.Body.position.x,this.Body.position.y,this.W,this.H);
  }
}
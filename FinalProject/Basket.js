/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/

class Basket
{
  constructor(x,y,w,h)
  {
    this.W = w;
    this.H = h;
    this.Img = loadImage('Images/Basket.png');
    this.Body = Bodies.rectangle(x,y,w,h,{collisionFilter:{category:CATEGORY_BASKET,mask : CATEGORY_PARTICLES|CATEGORY_LIQUORS|CATEGORY_ITEM},isSensor:true,mass:0.1,owner:this});
  }

  Draw()
  {
    image(this.Img,this.Body.position.x,this.Body.position.y,this.W,this.H);
  }
}


/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class Vector3
{
  constructor(x,y,z)
  {
    this.X = x;
    this.Y = y;
    this.Z = z;
  }
  
  CrossProduct(paramV)
  {
    return new Vector3(this.Y*paramV.Z-this.Z*paramV.Y,this.Z*paramV.X-this.X*paramV.Z,this.X-paramV.Y-this.Y*paramV.X);
  }
  
  DotProduct(paramV)
  {
    return (this.X*paramV.X+this.Y*paramV.Y+this.Z*paramV.Z);
  }
  
  GetLengthSqr()
  {
    return (pow(this.X,2)+pow(this.Y,2)+pow(this.Z,2));
  }
  
  GetLength()
  {
    return sqrt(pow(this.X,2)+pow(this.Y,2)+pow(this.Z,2));
  }
  
  Add(V2)
  {
    return new Vector3(this.X+V2.X,this.Y+V2.Y,this.Z+V2.Z);
  }
  
  Sub(V2)
  {
    return new Vector3(this.X-V2.X,this.Y-V2.Y,this.Z-V2.Z);
  }
  
  AddBy(V2)
  {
    this.X+=V2.X;
    this.Y+=V2.Y;
    this.Z+=V2.Z;
  }
  
  SubBy(V2)
  {
    this.X-=V2.X;
    this.Y-=V2.Y;
    this.Z-=V2.Z;
  }
  
  MultiplyScalar(s)
  {
    return new Vector3(this.X*s,this.Y*s,this.Z*s);
  }
  
  MultiplyByScalar(s)
  {
    this.X*=s;
    this.Y*=s;
    this.Z*=s;
  }
  
  DivideScalar(s)
  {
    return new Vector3(this.X/s,this.Y/s,this.Z/s);
  }
  
  DivideByScalar(s)
  {
    this.X/=s;
    this.Y/=s;
    this.Z/=s;
  }
  
  Set(x,y,z)
  {
    this.X = x;
    this.Y = y;
    this.Z = z;
  }
  
  Get()
  {
    return this;
  }
}
/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class Sword 
{
  constructor(x,y,w,h)
  {
    this.W = w;
    this.H = h;
    this.Img = loadImage('Images/Sword.png');
    this.Body = Bodies.rectangle(x,y,w,h,{angle:0,collisionFilter:{category:CATEGORY_SWORD,mask : CATEGORY_PARTICLES|CATEGORY_LIQUORS},mass:20,owner:this});
  }

  Draw()
  {
    push();
    translate(this.Body.position.x,this.Body.position.y);
    rotate(this.Body.angle);
    image(this.Img,0,0,this.W,this.H);
    pop();
  }
  
  Update()
  {
    Matter.Body.set(this.Body,{angularVelocity:this.Body.angularVelocity*0.9});
  }
}
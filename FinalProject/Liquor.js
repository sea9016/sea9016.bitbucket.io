/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class Liquor
{
  constructor(x,y,w,h,life,type)
  {
    this.Tag = 'Liquor';
    this.Life = life;
    this.Type = type;
    this.W = w;
    this.H = h;
    this.Img;
    this.IsHoldingItem;
    this.IsCollected;

    if(type == LIQUOR_TYPE.Bonus)
    {
      this.IsHoldingItem = false;
    }
    else
    {
      let IsHoldingItem = floor(random(0,5));
      if(IsHoldingItem == 0)
      {
         this.IsHoldingItem = true;
      }
      else
      {
        this.IsHoldingItem = false;
      }
    }

    switch(type)
    {
      case LIQUOR_TYPE.Soju:
      this.Img = loadImage('Images/Soju.png');
      break;
      case LIQUOR_TYPE.Maekju:
      this.Img = loadImage('Images/Maekju.png');
      break;
      case LIQUOR_TYPE.Sake:
      this.Img = loadImage('Images/Sake.png');
      break;
      case LIQUOR_TYPE.Wine:
      this.Img = loadImage('Images/Wine.png');
      break;
      case LIQUOR_TYPE.SogokJu:
      this.Img = loadImage('Images/Sogokju.png');
      break;
      case LIQUOR_TYPE.Whiskey:
      this.Img = loadImage('Images/Whiskey.png');
      break;
      case LIQUOR_TYPE.Bonus:
      this.Img = loadImage('Images/BonusLiquor.png');
      break;
    }
    this.Body = Bodies.rectangle(x,y,w,h,{collisionFilter:{category:CATEGORY_LIQUORS,mask:CATEGORY_PARTICLES|CATEGORY_SWORD|CATEGORY_LIQUORS|CATEGORY_GROUND|CATEGORY_BASKET},mass:5,owner:this});
  }

  Draw()
  {
     // image(this.Img,this.X,this.Y,this.W,this.H); real code.
    push();
    translate(this.Body.position.x,this.Body.position.y);
    rotate(this.Body.angle);
    image(this.Img,0,0,this.W,this.H);
    pop();
  }

  Update()
  {
    if(this.Body.position.x>width&&this.Body.velocity.x>0||this.Body.position.x<0&&this.Body.velocity.x<0)
    {
      Matter.Body.set(this.Body,{velocity:{x:this.Body.velocity.x*-1,y:this.Body.velocity.y}});
      this.DecreaseLife();
    }
  }

  DecreaseLife()
  {
    if(this.Life > 0)
    {
      this.Life--;
    }
    else
    {
      this.Life = 0;
    }
  }

}

  
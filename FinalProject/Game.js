/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class Game
{
  constructor()
  {
    this.State = STATE.MainMenu;
    this.Life = 500;
    this.HighScore = 0;
  }

  CheckState()
  {
  	if(this.Life<=0)
  	{
      this.State = STATE.GameOver;
      let Score = millis()-PrevTime;
      if(Score>this.HighScore)
      {
        this.HighScore = Score;
      }
  	}
  }

  InitGame()
  {
    PrevTime = millis();
    this.Life = 500;
    this.State = STATE.GamePlaying;
    _ObjectManager.Reset();
    _ParticleManager.Reset();
    Matter.Body.set(_Player.Body,{position:{x:width/2,y:_Player.Body.position.y}});
    // player 에도 리셋함수 만들어서 부르기.
  }
}
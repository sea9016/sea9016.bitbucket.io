/*

Doyoon Kim (김도윤)

assignment : Prototype

CS099

Spring 2019

*/


class UI
{
  constructor()
  {
    this.PlayerNormalImage = loadImage('Images/Player_profile_normal.png');
    this.PlayerHappyImage1 = loadImage('Images/Player_profile_happy1.png');
    this.PlayerHappyImage2 = loadImage('Images/Player_profile_happy2.png');
    this.PlayerMadImage1 = loadImage('Images/Player_profile_mad1.png');
    this.PlayerMadImage2 = loadImage('Images/Player_profile_mad2.png');

    this.GameStartButton = createButton('GameStart');
    this.ScoreButton = createButton('Score');
    this.ReplayButton = createButton('Replay');
    this.HowToPlayButton = createButton('HowToPlay');
    this.BackToMenuButton = createButton('BackToMenu');
    this.ReplayButton.hide();
    this.BackToMenuButton.hide();

    this.GameStartButton.position(width/2-width/8,height/4-height/16);
    this.ScoreButton.position(width/2-width/8,height/2-height/16);
    this.HowToPlayButton.position(width/2-width/8,height*3/4-height/16);
    this.ReplayButton.position(width/2-width/4,height/2-height/12);
    this.BackToMenuButton.position(width-width/8,0);

    this.GameStartButton.size(width/4,height/8);
    this.ScoreButton.size(width/4,height/8);
    this.HowToPlayButton.size(width/4,height/8);
    this.ReplayButton.size(width/2,height/6);
    this.BackToMenuButton.size(width/8,height/10);

    this.GameStartButton.mouseClicked(function(){
    	_Game.InitGame();
    	_Game.State = STATE.GamePlaying;
    	_UI.GameStartButton.hide();
    	_UI.ScoreButton.hide();
    	_UI.HowToPlayButton.hide();
    	PrevTime = millis();
    });

    this.ScoreButton.mouseClicked(function(){
        _Game.State = STATE.Score;
        _UI.GameStartButton.hide();
        _UI.ScoreButton.hide();
        _UI.HowToPlayButton.hide();
        _UI.BackToMenuButton.show();
    });
    
    this.HowToPlayButton.mouseClicked(function(){
        _Game.State = STATE.HowToPlay;
        _UI.GameStartButton.hide();
        _UI.ScoreButton.hide();
        _UI.HowToPlayButton.hide(); 
        _UI.BackToMenuButton.show();
    });

    this.BackToMenuButton.mouseClicked(function(){
        _Game.State = STATE.MainMenu;
        _UI.GameStartButton.show();
        _UI.ScoreButton.show();
        _UI.HowToPlayButton.show();
        _UI.BackToMenuButton.hide();
        _UI.ReplayButton.hide();
    });

    this.ReplayButton.mouseClicked(function(){
        _UI.ReplayButton.hide();
        _UI.BackToMenuButton.hide();
        _Game.InitGame();
    });
  }
  
  Draw()
  {
    switch(_Game.State)
    {
    	case STATE.MainMenu:
    	break;
    	case STATE.GamePlaying:
    	//게임 플레이 화면 UI 
        rect(width/2,height-100,width,200);
        this.DrawLifePoint();
        this.DrawPlayerFace();
    	break;
    	case STATE.GameOver:
        //
    	break;
    	case STATE.AskIfReplay:
    	break;
    	case STATE.Score:
    	push();
    	textAlign(CENTER);
    	textSize(40);
    	text(round(_Game.HighScore/1000)+' Seconds',width/2,height/2);
    	pop();
    	break;
    	case STATE.HowToPlay:
      image(HowToPlayImg,width/2,height/2,width,height);
    	break;
    	default:
    	break;
    }
  }

  DrawLifePoint()
  {
    push();
    textAlign(CENTER);
    textSize(30);
    text('Life : '+_Game.Life,width/4,height-100);
    pop();
  }

  DrawPlayerFace()
  {
    let Duration = _Player.EmotionDuration;
    switch(_Player.Emotion)
    {
      case Emotion.Normal:
      image(this.PlayerNormalImage,width*3/4,height-100,width/4,200);
      break;
      case Emotion.Happy:
      
      if(Duration>0&&Duration<=10||Duration>20&&Duration<=30||Duration>40&&Duration<=50||Duration>60&&Duration<=70||Duration>80&&Duration<=90)
      {
        image(this.PlayerHappyImage1,width*3/4,height-100,width/4,200);
      }
      else{
        image(this.PlayerHappyImage2,width*3/4,height-100,width/4,200);
      } 
      break;
      case Emotion.Mad:
      if(Duration>0&&Duration<=10||Duration>20&&Duration<=30||Duration>40&&Duration<=50||Duration>60&&Duration<=70||Duration>80&&Duration<=90)
      {
        image(this.PlayerMadImage1,width*3/4,height-100,width/4,200);
      }
      else{
        image(this.PlayerMadImage2,width*3/4,height-100,width/4,200);
      }
      break; 
    }
  }

}
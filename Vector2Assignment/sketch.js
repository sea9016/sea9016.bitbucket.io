/*

Doyoon Kim (김도윤)

assignment : Vec2 class assignment

CS099

Spring 2019

*/



// y increases from the bottom.
class Vector2
{
  constructor(x,y,name)//x y is coordinate
  {
    this.Name = name;
    this.X = x+width/2;
    this.Y = height/2-y;
    this.Length = sqrt(pow(width/2-this.X,2)+pow(height/2-this.Y,2));
    this.Angle = atan2(height/2-this.Y,this.X-width/2);
  }
  
  Draw()
  {
    line(width/2,height/2,this.X,this.Y);
    push();
    translate(this.X,this.Y);
    rotate(-this.Angle);
    triangle(0,-5,0,5,5,0);
    pop();
    text(this.Name+' '+round(this.GetAngle())+' '+round(this.Length),width/2+(this.X-width/2)/2,height/2+(this.Y-height/2)/2);
  }

  GetAngle()
  {
    return this.Angle/PI*180;
  }

  SetAngle(radian)
  {
    this.X = width/2+this.Length*cos(radian);
    this.Y = height/2-this.Length*sin(radian);
    this.Angle = radian%TWO_PI;
  }

  GetLength()
  {
    return this.Length;
  }

  SetLength(length)
  {
    this.Length = length;
    this.X = width/2+length*cos(this.Angle);
    this.Y = height/2-length*sin(this.Angle);
  }

  Add(V2,name = this.Name)
  {
    return new Vector2((this.X-width/2)+(V2.X-width/2),height/2-(height/2-this.Y)-(height/2-V2.Y),name);
  }

  AddTo(V2,name = this.Name)
  {
    this.Name = name;
    this.X+=V2.X;
    this.Y-=(height/2-V2.Y)
    this.Length = sqrt(pow(this.X-width/2,2)+pow(height/2-this.Y,2));
    this.Angle = -atan2(height/2-(height-this.Y),this.X-width/2);
  }

  Subtract(V2,name = this.Name)
  {
    return new Vector2((this.X-width/2)-(V2.X-width/2),(height/2-this.Y)-(height/2-V2.Y),name);
  }

  SubtractFrom(V2,name = this.Name)
  {
    this.Name;
    this.X-=V2.X-width/2;
    this.Y+=(height/2-V2.Y);
    this.Length = sqrt(pow(this.X-width/2,2)+pow(height/2-this.Y,2));
    this.Angle = -atan2(height/2-(height-this.Y),this.X-width/2);
  }

  Multiply(scalar,name = this.Name)
  {
    return new Vector2((this.X-width/2)*scalar,(height/2-this.Y)*scalar,name);
  }

  MultiplyBy(scalar,name = this.Name)
  {
    this.Name = name;
    this.X = width/2+(this.X-width/2)*scalar;
    this.Y = height/2-(height/2-this.Y)*scalar;
  }

  Divide(scalar,name = this.Name)
  {
    return new Vector2((this.X-width/2)/scalar,(height/2-this.Y)/scalar,name);
  }

  DivideBy(scalar,name = this.Name)
  {
    this.Name = name;
    this.X = width/2+(this.X-width/2)/scalar;
    this.Y = height/2-(height/2-this.Y)/scalar;
  }
}

let Vector2Arr = new Array();
let PrevWindowWidth;
let PrevWindowHeight;

function setup() {
 createCanvas(800, 800);
 Vector2Arr[0] = new Vector2(100,50,'A');
 Vector2Arr[1] = new Vector2(50,100,'B');
 Vector2Arr[0].MultiplyBy(2,'2A');
 Vector2Arr[2] = Vector2Arr[0].Add( Vector2Arr[1],'2A+B'); 

 Vector2Arr[3] = new Vector2(-400,-200,'C');
 Vector2Arr[4] = new Vector2(-100,-300,'D');
 Vector2Arr[3].DivideBy(2,'C/2');
 Vector2Arr[5] = Vector2Arr[3].Subtract( Vector2Arr[4],'C/2-D');

 Vector2Arr[6] = new Vector2(-100,-100,'MouseVector');
  PrevWindowWidth = windowWidth;
  PrevWindowHeight = windowHeight;
}

function draw() {
  background(220);

  fill(255,0,0);
  stroke(255,0,0);
   Vector2Arr[0].Draw();

  fill(0,0,255);
  stroke(0,0,255);
   Vector2Arr[1].Draw();

  fill(0);
  stroke(0);
   Vector2Arr[2].Draw();

  fill(0,180,0);
  stroke(0,180,0);
   Vector2Arr[3].Draw();

  fill(200,200,0);
  stroke(200,200,0);
   Vector2Arr[4].Draw();

  fill(0,200,200);
  stroke(0,200,200);
   Vector2Arr[5].Draw();

  fill(255,100,255);
  stroke(255,100,255);
   Vector2Arr[6].SetAngle(map(mouseX,0,width,0,TWO_PI,true))
   Vector2Arr[6].SetLength(map(mouseY,height,0,0,height,true));
   Vector2Arr[6].Draw();
  PrevWindowWidth = windowWidth;
  PrevWindowHeight = windowHeight;
}



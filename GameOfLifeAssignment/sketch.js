/*

Doyoon Kim (김도윤)

assignment : Game Of Life

CS099

Spring 2019

*/


function setup() {
  createCanvas(400, 400);
  SetGame();
}

function draw() {
  background(220);
  _Grid.UpdateGrid();
  _Grid.DrawGrid();
  setTimeout(draw,150);
  noLoop();
}

function SetGame()
{
  for(let y = 0; y<nRow; ++y)
  {
    _Grid.Grid.push([]);
    _Grid.NextGrid.push([]);
    for(let x = 0; x<nColumn; ++x)
    {
      _Grid.Grid[y].push([]);
      _Grid.NextGrid[y].push([]);
      _Grid.Grid[y][x] = floor(random(0,2));//0,1
    }
  }
  
  _Grid.ResetGrid(_Grid.NextGrid);
}

function keyPressed()
{
  if(keyIsDown(32))//spacebar
  {
   _Spawner.SpawnStillLife();
  }

  if(keyIsDown(82))//r
  {
   _Grid.SetRandomGrid(_Grid.Grid);
   _Grid.ResetGrid(_Grid.NextGrid);
  }

  if(keyIsDown(67))//c
  {
   _Grid.ResetGrid(_Grid.Grid);
   _Grid.ResetGrid(_Grid.NextGrid);
  }

}

function mousePressed()
{
  if(mouseButton === LEFT)
  {
   _Spawner.SpawnOscillators(mouseX,mouseY);
  }
  else if(mouseButton === CENTER)
  {
   _Spawner.SpawnSpaceShips(mouseX,mouseY);
  }
}
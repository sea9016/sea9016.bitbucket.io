class Grid
{
  constructor()
  {
    this.Grid = [];
    this.NextGrid = [];
  }
  
  ResetGrid(grid)
  {
   for(let y = 0; y<nRow; y++)
   {
     for(let x = 0; x<nColumn; x++)
     {
       grid[y][x] = 0;
     }
   }
  }
  
  SetRandomGrid(grid)
  {
   for(let i = 0; i<nRow; ++i)
   {
    for(let j = 0; j<nColumn; ++j)
    {
      grid[i][j] = floor(random(0,2));//0,1
    }
   }
  }
  
  CopyGrid(src,dst)
  {
   for(let y = 0; y<nRow; y++)
   {
    for(let x = 0; x<nColumn; x++)
    {
      dst[y][x] = src[y][x];
    }
   }
  }
  
  UpdateGrid()
  {
   for(let y = 0; y<nRow;++y)
   {
    for(let x = 0; x<nColumn;++x)
    {
      let AliveCount = 0;
      if(y != 0 && y != nRow-1)
      {
       if(x != 0 && x!= nColumn-1)
        {
          AliveCount += this.Grid[y-1][x-1];
          AliveCount += this.Grid[y-1][x];
          AliveCount += this.Grid[y-1][x+1];
          AliveCount += this.Grid[y][x-1];
          AliveCount += this.Grid[y][x+1];
          AliveCount += this.Grid[y+1][x-1];
          AliveCount += this.Grid[y+1][x];
          AliveCount += this.Grid[y+1][x+1];
        }
        else if(x == 0)
        {
         AliveCount += this.Grid[y-1][x];
         AliveCount += this.Grid[y-1][x+1];
         AliveCount += this.Grid[y][x+1];
         AliveCount += this.Grid[y+1][x];
         AliveCount += this.Grid[y+1][x+1]; 
        }
        else if(x == nColumn-1)
        {
         AliveCount += this.Grid[y-1][x-1];
         AliveCount += this.Grid[y-1][x];
         AliveCount += this.Grid[y][x-1];
         AliveCount += this.Grid[y+1][x-1];
         AliveCount += this.Grid[y+1][x];
        }
      }
      else if(y == 0)
      {
        if(x != 0 && x!= nColumn-1)
        {
         AliveCount += this.Grid[y][x-1];
         AliveCount += this.Grid[y][x+1];
         AliveCount += this.Grid[y+1][x-1];
         AliveCount += this.Grid[y+1][x];
         AliveCount += this.Grid[y+1][x+1]; 
        }
        else if(x == 0)
        {
         AliveCount += this.Grid[y][x+1];
         AliveCount += this.Grid[y+1][x];
         AliveCount += this.Grid[y+1][x+1]; 
        }
        else if(x == nColumn-1)
        {
         AliveCount += this.Grid[y][x-1];
         AliveCount += this.Grid[y+1][x-1];
         AliveCount += this.Grid[y+1][x]; 
        }
      }
      else // i == nRow-1
      {
        if(x != 0 && x!= nColumn-1)
        {
         AliveCount += this.Grid[y-1][x-1];
         AliveCount += this.Grid[y-1][x];
         AliveCount += this.Grid[y-1][x+1];
         AliveCount += this.Grid[y][x-1];
         AliveCount += this.Grid[y][x+1]; 
        }
        else if(x == 0)
        {
         AliveCount += this.Grid[y-1][x];
         AliveCount += this.Grid[y-1][x+1];
         AliveCount += this.Grid[y][x+1]; 
        }
        else if(x == nColumn-1)
        {
         AliveCount += this.Grid[y-1][x-1];
         AliveCount += this.Grid[y-1][x];
         AliveCount += this.Grid[y][x-1]; 
        }
      }
        
      if(this.Grid[y][x])
      {
        (AliveCount==2||AliveCount==3)?(this.NextGrid[y][x] = 1):(this.NextGrid[y][x] = 0);
      }
      else
      {
        if(AliveCount == 3)
        {
          this.NextGrid[y][x] = 1;
        }
      }
    }
  }
  this.CopyGrid(this.NextGrid,this.Grid);
  this.ResetGrid(this.NextGrid);
  }
  
  DrawGrid()
  {
   for(let y = 0; y<nRow; ++y)
   {
    for(let x = 0; x<nColumn; ++x)
    {
     fill(!this.Grid[y][x]*255);
     rect(width/nColumn*x,height/nRow*y,width/nColumn,height/nRow);
    }
   }
  }
  
}

let _Grid = new Grid();
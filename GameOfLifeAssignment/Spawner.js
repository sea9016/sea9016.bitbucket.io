class Spawner
{
  constructor()
  {

  }
  
  SpawnStillLife()
  {
    let n = floor(random(0,5))
    switch(n)
    {
      case 0:
       SpawnBlock();
       print('0');
       break;
      case 1:
       SpawnBeeHive();
       print('1');
       break;
      case 2:
       SpawnLoaf();
       print('2');
       break;
      case 3:
      SpawnBoat();
      print('3');
       break;
      case 4:
      SpawnTub();
      print('4');
       break;
      default:
       break;
    }
  }
  
  SpawnOscillators(x,y)
  {
    let n = floor(random(0,5));
    switch(n)
    {
      case 0:
       SpawnBlinker(x,y);
       print('0');
       break;
      case 1:
       SpawnToad(x,y);
       print('1');
       break;
      case 2:
       SpawnBeacon(x,y);
       print('2');
       break;
      case 3:
       SpawnPulasr(x,y);
       print('3');
       break;
      case 4:
       SpawnPentaDecathlon(x,y);
       print('4');
       break;
      default:
       break;
    }
  }
  
  SpawnSpaceShips(x,y)
  {
    let n = floor(random(0,4));
     switch(n)
     {
      case 0:
       SpawnGlider(x,y);
       print('0');
       break;
      case 1:
       SpawnL_W_SpaceShip(x,y);
       print('1');
       break;
      case 2:
       SpawnM_W_SpaceShip(x,y);
       print('2');
       break;
      case 3:
       SpawnH_W_SpaceShip(x,y);
       print('3');
       break;
      default:
       break;
     }
  }
}




function SpawnBlock()
{
  let x = floor(random(0,nColumn-1));
  let y = floor(random(0,nRow-1));
  _Grid.Grid[y][x]= 1;
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+1] = 1;
}

function SpawnBeeHive()
{
  let x = floor(random(0,nColumn-3));
  let y = floor(random(0,nRow-2));
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+3] = 1;
  _Grid.Grid[y+2][x+1] = 1;
  _Grid.Grid[y+2][x+2] = 1;
}

function SpawnLoaf()
{
  let x = floor(random(0,nColumn-3));
  let y = floor(random(0,nRow-3));
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+3] = 1;
  _Grid.Grid[y+2][x+1] = 1;
  _Grid.Grid[y+2][x+3] = 1;
  _Grid.Grid[y+3][x+2] = 1;
}

function SpawnBoat()
{
  let x = floor(random(0,nColumn-2));
  let y = floor(random(0,nRow-2));
  _Grid.Grid[y][x] = 1;
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+2] = 1;
  _Grid.Grid[y+2][x+1] = 1;
}

function SpawnTub()
{
  let x = floor(random(0,nColumn-2));
  let y = floor(random(0,nRow-2));
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+2] = 1;
  _Grid.Grid[y+2][x+1] = 1;
}
/////////////////////////////////////////////////////////////
function SpawnBlinker(x,y)
{
  x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+2][x] = 1;
}

function SpawnToad(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+3] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+2][x+3] = 1;
  _Grid.Grid[y+3][x+1] = 1;
}

function SpawnBeacon(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x] = 1;
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+1] = 1;
  _Grid.Grid[y+2][x+2] = 1;
  _Grid.Grid[y+2][x+3] = 1;
  _Grid.Grid[y+3][x+2] = 1;
  _Grid.Grid[y+3][x+3] = 1;
}

function SpawnPulasr(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y][x+3] = 1;
  _Grid.Grid[y][x+9] = 1;
  _Grid.Grid[y][x+10] = 1;
  _Grid.Grid[y+1][x+3] = 1;
  _Grid.Grid[y+1][x+4] = 1;
  _Grid.Grid[y+1][x+8] = 1;
  _Grid.Grid[y+1][x+9] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+2][x+3] = 1;
  _Grid.Grid[y+2][x+5] = 1;
  _Grid.Grid[y+2][x+7] = 1;
  _Grid.Grid[y+2][x+9] = 1;
  _Grid.Grid[y+2][x+12] = 1;
  _Grid.Grid[y+3][x] = 1;
  _Grid.Grid[y+3][x+1] = 1;
  _Grid.Grid[y+3][x+2] = 1;
  _Grid.Grid[y+3][x+4] = 1;
  _Grid.Grid[y+3][x+5] = 1;
  _Grid.Grid[y+3][x+7] = 1;
  _Grid.Grid[y+3][x+8] = 1;
  _Grid.Grid[y+3][x+10] = 1;
  _Grid.Grid[y+3][x+11] = 1;
  _Grid.Grid[y+3][x+12] = 1;
  _Grid.Grid[y+4][x+1] = 1;
  _Grid.Grid[y+4][x+3] = 1;
  _Grid.Grid[y+4][x+5] = 1;
  _Grid.Grid[y+4][x+7] = 1;
  _Grid.Grid[y+4][x+9] = 1;
  _Grid.Grid[y+4][x+11] = 1;
  _Grid.Grid[y+5][x+2] = 1;
  _Grid.Grid[y+5][x+3] = 1;
  _Grid.Grid[y+5][x+4] = 1;
  _Grid.Grid[y+5][x+8] = 1;
  _Grid.Grid[y+5][x+9] = 1;
  _Grid.Grid[y+5][x+10] = 1;
  for(let i = 0; i<6;++i)// ㅅㅂ 사분면 하나만 만들어서 대칭하면 되는걸 절반이나 만들어놓고 꺠닳아버림 ㅉㅉ
  {
    for(let j = 0;j<13;++j)
    {
      _Grid.Grid[y+7+i][x+j] = _Grid.Grid[y+5-i][x+j];
    }
  } 
}

function SpawnPentaDecathlon(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x] = 1;
  _Grid.Grid[y][x+1] = 1;
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+2] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+2][x+1] = 1;
  _Grid.Grid[y+2][x+2] = 1;
  _Grid.Grid[y+3][x] = 1;
  _Grid.Grid[y+3][x+1] = 1;
  _Grid.Grid[y+3][x+2] = 1;
  _Grid.Grid[y+4][x] = 1;
  _Grid.Grid[y+4][x+1] = 1;
  _Grid.Grid[y+4][x+2] = 1;
  _Grid.Grid[y+5][x] = 1;
  _Grid.Grid[y+5][x+1] = 1;
  _Grid.Grid[y+5][x+2] = 1;
  _Grid.Grid[y+6][x] = 1;
  _Grid.Grid[y+6][x+2] = 1;
  _Grid.Grid[y+7][x] = 1;
  _Grid.Grid[y+7][x+1] = 1;
  _Grid.Grid[y+7][x+2] = 1;
}
//////////////////////////////////////////////////
function SpawnGlider(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x] = 1;
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y+1][x+1] = 1;
  _Grid.Grid[y+1][x+2] = 1;
  _Grid.Grid[y+2][x+1] = 1;
}

function SpawnL_W_SpaceShip(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x+2] = 1;
  _Grid.Grid[y][x+3] = 1;
  _Grid.Grid[y+1][x] = 1;
  _Grid.Grid[y+1][x+1] = 1;
  _Grid.Grid[y+1][x+3] = 1;
  _Grid.Grid[y+1][x+4] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+2][x+1] = 1;
  _Grid.Grid[y+2][x+2] = 1;
  _Grid.Grid[y+2][x+3] = 1;
  _Grid.Grid[y+3][x+1] = 1;
  _Grid.Grid[y+3][x+2] = 1;
}

function SpawnM_W_SpaceShip(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x+3] = 1;
  _Grid.Grid[y+1][x+1] = 1;
  _Grid.Grid[y+1][x+5] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+3][x] = 1;
  _Grid.Grid[y+3][x+5] = 1;
  _Grid.Grid[y+4][x] = 1;
  _Grid.Grid[y+4][x+1] = 1;
  _Grid.Grid[y+4][x+2] = 1;
  _Grid.Grid[y+4][x+3] = 1;
  _Grid.Grid[y+4][x+4] = 1;
}

function SpawnH_W_SpaceShip(x,y)
{
   x = floor(x/nColumn);
  y = floor(y/nRow);
  _Grid.Grid[y][x+3] = 1;
  _Grid.Grid[y][x+4] = 1;
  _Grid.Grid[y+1][x+1] = 1;
  _Grid.Grid[y+1][x+6] = 1;
  _Grid.Grid[y+2][x] = 1;
  _Grid.Grid[y+3][x] = 1;
  _Grid.Grid[y+3][x+6] = 1;
  _Grid.Grid[y+4][x] = 1;
  _Grid.Grid[y+4][x+1] = 1;
  _Grid.Grid[y+4][x+2] = 1;
  _Grid.Grid[y+4][x+3] = 1;
  _Grid.Grid[y+4][x+4] = 1;
  _Grid.Grid[y+4][x+5] = 1;
}

let _Spawner = new Spawner();

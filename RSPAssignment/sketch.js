/*

Doyoon Kim (김도윤)

assignment : Rock Scissors Paper

CS099

Spring 2019

*/


const STATE = {Player1Choose:0,Player2Choose:1,ShowResult:2,ChooseToReplay:3};

const CHOICE = {Rock:0,Scissors:1,Paper:2};

let Choice = {Player1Choice:0,Player2Choice:0};

let state = STATE.Player1Choose;

let InitCountDown = 300;

let CountDown = InitCountDown;

let InitReplayCountDown = 180;

let ReplayCountDown = InitReplayCountDown;

let PaperScissorsRockImage = null;

let RockImage = null;

let ScissorsImage = null;

let PaperImage = null;

function setup() {
  createCanvas(600, 600);
  
  PaperScissorsRockImage = loadImage('paper scissors rock.png');
  RockImage = loadImage('Rock.png');
  ScissorsImage = loadImage('Scissors.png');
  PaperImage = loadImage('Paper.png');
  
  imageMode(CENTER);
  textAlign(CENTER);
  textSize(width*height/5000);
}

function draw() {
  background(220);
  //player 1 choose
  //player 2 choose
  //reveal the result.show each player's choose and and who won
  //show replay button and if it's pressed, play again.
  switch(state)
  {
    case STATE.Player1Choose:
      DrawPlayer1Choice();
      GetP1Input(Choice);
      break;
    case STATE.Player2Choose:
      DrawPlayer2Choice();
      GetP2Input(Choice);
      break;
    case STATE.ShowResult:
      ShowResult(CheckWinner(Choice));
      break;
    case STATE.ChooseToReplay:
      ChooseToReplay();
      break;
    default:
      break;
  }
}

function GetP1Input(Choice)
{
  Choice.Player1Choice = TakePlayer1Choice(keyCode);
}

function GetP2Input(Choice)
{
  Choice.Player2Choice = TakePlayer2Choice(keyCode);
}

function TakePlayer1Choice(key)
{
  switch(key)
      {
        case 65://A
          state = STATE.Player2Choose;
          return CHOICE.Rock;
        case 83://S
         state = STATE.Player2Choose;
          return CHOICE.Paper;
        case 68://D
          state = STATE.Player2Choose;
          return CHOICE.Scissors;
        default:
          break;
      }
}

function TakePlayer2Choice(key)
{
   switch(key)
      {
        case 72://H
          state = STATE.ShowResult;
          return CHOICE.Rock;
        case 74://J
          state = STATE.ShowResult;
          return CHOICE.Paper;
        case 75://K
          state = STATE.ShowResult;
          return CHOICE.Scissors;
          default:
        break;
      }
  
}

function DrawHand(choice,x,y,w,h)
{
  switch(choice)
  {
    case CHOICE.Paper:
      image(PaperImage,x,y,w,h);
      break;
    case CHOICE.Scissors:
      image(ScissorsImage,x,y,w,h);
      break;
    case CHOICE.Rock:
      image(RockImage,x,y,w,h);
      break;
    default:
      break;
  }
}

function DrawPlayer1Choice()
{
  image(PaperScissorsRockImage,width/2,height/2,width*3/4,height/4);
  text("Player 1 Choose",width/2,height*4/5);
}

function DrawPlayer2Choice()
{
  image(PaperScissorsRockImage,width/2,height/2,width*3/4,height/4);
  text("Player 2 Choose",width/2,height*4/5);
}

function IsCountDownOver(CountDown)
{
  if(CountDown>0)
  {
    push();
    textSize(50);
    text(round(CountDown/60),width/2,height/2);
    pop();
    return 0;
  }
  else
  {
    return 1;
  }
}

function CheckWinner(choice)
{
  switch(choice.Player1Choice-choice.Player2Choice)
  {
    case -2:
      return 'Player2';
    case -1:
      return 'Player1';
    case 0:
      return 'Draw';
    case 1:
      return 'Player2';
    case 2:
      return 'Player1';
    default:
      break;
  }
}

function ShowResult(Winner)
{
  CountDown--;
  if(IsCountDownOver(CountDown))
  {
    if(Winner == 'Draw')
    {
     push();
     textSize(50);
     text('Draw',width/2,height/2);
     pop();
    }
    else 
    {
     push();
     textSize(50);
     text(Winner+'Win!',width/2,height/2);
     pop();
    }
    DrawHand(Choice.Player1Choice,width/4,height-height/4,width/5,height/5);
    DrawHand(Choice.Player2Choice,width-width/4,height-height/4,width/5,height/5);
    ReplayCountDown--;
    if(ReplayCountDown == 0)
    {
      state = STATE.ChooseToReplay;
    }
  }
}

function ChooseToReplay()
{
  push();
  textSize(50);
  text('Replay?',width/2,height/2);
  pop();
  if(keyIsPressed)
  {
     InitGame();
  }
}

function InitGame()
{
  CountDown = InitCountDown;
  ReplayCountDown = InitReplayCountDown;
  state = STATE.Player1Choose;
}